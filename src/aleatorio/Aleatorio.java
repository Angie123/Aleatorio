/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aleatorio;

/**
 *
 * @Angélica Reyes y Esther Cornejo
 */
public class Aleatorio {
    public static int ceroACien(){
      return(int)(Math.random()*100);  
    }
    public static int n_A_M(int x, int y ){
        double prob= 1.0/(y-x);
        double res=Math.random()/prob;
        res +=x;//res=res +x;
        if((int) (res + 0.5)> (int)res ){
            return(int)res+1;
        }
        else{
            return(int)res;
            
        }
        
    }
    public static double n_A_M(double n, double m, int dec){
        double d=Math.pow(10, dec);
        double prob= 1.0/(m-n)*d;
        double res=(1.0/dec)*(Math.random()/prob);
        res +=n;//res=res+n
        
        String num=Double.toString(res);
        return Double.parseDouble(num.substring(0,num.indexOf('.')+dec+1));
        
    }

    
    public static void main(String[] args) {
        // TODO code application logic here
         int a, b;
        a=13;
        b=700;
        int v1=Aleatorio.ceroACien();
        int v2=Aleatorio.n_A_M(a,b);
        System.out.println("Valor aleatorio entre 0 y 100: "+v1);
        System.out.println("Valor aleatorio entre "+a+" y "+b+": "+v2);
    }
    
}
